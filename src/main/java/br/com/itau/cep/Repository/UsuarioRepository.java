package br.com.itau.cep.Repository;


import br.com.itau.cep.model.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
