package br.com.itau.cep.Controller;


import br.com.itau.cep.DTO.UsuarioCepDTO;
import br.com.itau.cep.Service.UsuarioService;
import br.com.itau.cep.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsuarioController {

@Autowired
    UsuarioService usuarioService;

    @GetMapping("/{nome}/{cep}")
     public Usuario criarUsuario(@PathVariable String nome,@PathVariable String cep) {

        UsuarioCepDTO usuarioCepDTO = new UsuarioCepDTO();

        usuarioCepDTO.setCep(cep);
        usuarioCepDTO.setNome(nome);

        return usuarioService.salvarUsuario(usuarioCepDTO);

    }

}
