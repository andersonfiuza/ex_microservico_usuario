package br.com.itau.cep.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name= "CONSULTACEP")
public interface UsuarioCep {

    @GetMapping("/{cep}")
    Cep buscarCep(@PathVariable String cep);


}
