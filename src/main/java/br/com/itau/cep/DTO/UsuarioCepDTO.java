package br.com.itau.cep.DTO;

import br.com.itau.cep.client.UsuarioCep;
import br.com.itau.cep.model.Usuario;

public class UsuarioCepDTO {

    public String cep;


    private  String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

}
