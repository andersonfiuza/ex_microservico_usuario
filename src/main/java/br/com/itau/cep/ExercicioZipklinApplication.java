package br.com.itau.cep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ExercicioZipklinApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExercicioZipklinApplication.class, args);
	}

}
