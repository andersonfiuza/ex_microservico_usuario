package br.com.itau.cep.Service;


import br.com.itau.cep.DTO.UsuarioCepDTO;
import br.com.itau.cep.Repository.UsuarioRepository;
import br.com.itau.cep.client.Cep;
import br.com.itau.cep.client.UsuarioCep;
import br.com.itau.cep.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    UsuarioCep usuarioCep;


    public Usuario salvarUsuario(UsuarioCepDTO usuarioCepDTO) {


        Cep cep = usuarioCep.buscarCep(usuarioCepDTO.cep);

        Usuario usuario = new Usuario();

        usuario.setEndereco(cep.getLogradouro());
        usuario.setCep(usuarioCepDTO.getCep());
        usuario.setNome(usuarioCepDTO.getNome());

       return usuarioRepository.save(usuario);

    }
}
